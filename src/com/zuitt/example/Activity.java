package com.zuitt.example;

import java.util.Scanner;

public class Activity {
    public static void main(String[] args){

//        initialize variables
        String firstName, lastName;
        double firstSubject, secondSubject, thirdSubject;

        Scanner input = new Scanner(System.in);

        System.out.println("First Name:");
        firstName = input.nextLine();

        System.out.println("Last Name:");
        lastName = input.nextLine();

        System.out.println("First Subject Grade:");
        firstSubject = input.nextDouble();

        System.out.println("Second Subject Grade:");
        secondSubject = input.nextDouble();

        System.out.println("Third Subject Grade:");
        thirdSubject = input.nextDouble();

        double average = (firstSubject + secondSubject + thirdSubject)/3;

        System.out.println("Good day, " + firstName + " " + lastName + ".");
        System.out.println("Your average grade is " + average);

    }
}
