package com.zuitt.example;

import java.util.Scanner;

public class UserInput {
    public static void main(String[] args){
        /*
        Scanner class
        is a class in Java which allows us to create an instance that accepts input from the terminal.
        Acts like prompt() in JS.
        However, Scanner being a class predefined by Java has to be imported to be used.
         */

        Scanner myObj = new Scanner(System.in);
        System.out.println("Enter username: ");

//       nextLine() reads the input given by the user, returns a string
        String username = myObj.nextLine();
        System.out.println("Username is " + username);
    }
}
